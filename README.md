# Variant Calling for Whole-genome Sequencing of the Jurkat Cell Line

## Manuscript
[bioRxiv](http://biorxiv.org/)

## Available data:
### Sequencing
[SRA Study SRP101994](https://www.ncbi.nlm.nih.gov/sra/SRP101994)

### Variants
[Zenodo](https://zenodo.org/record/400615)

## Ordered list of scripts used in analysis:

### Alignment
#### [BWA-MEM](http://bio-bwa.sourceforge.net/)  

1. alignment/align_and_dedup.sh

### Variant Calling
#### [GATK](https://software.broadinstitute.org/gatk/)
variant_calling/gatk/  

1. realign_and_recalibrate.sh
2. merge_lanes.sh
3. picard_merged_preprocessing.sh
4. haplotype_caller.sh
5. vqsr.sh
6. filter_and_annotate.sh

#### [Pindel](http://gmt.genome.wustl.edu/packages/pindel/)
variant_calling/pindel/  
An example config file is provided: jurkat_config.txt  

1. run_pindel.sh
2. format_output.sh
3. filter_and_merge.sh

#### [BreakDancer](http://gmt.genome.wustl.edu/packages/breakdancer/)
variant_calling/breakdancer/  

1. 

#### [CNVnator](https://github.com/abyzovlab/CNVnator)
variant_calling/cnvnator/  

1. 

### Variant Call Merging
### [bedtools](http://bedtools.readthedocs.io/en/latest/)
variant_merging/  

1. merge_del.sh
