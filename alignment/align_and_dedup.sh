#!/bin/bash
#### Align jurkat fastq to hg19 reference ####

## exit on error
set -o errexit
## exit on unset variables
set -o nounset

## load software using environment modules
module load bwa/0.7.10
module load picard/1.103
module load samtools/1.0

## set directories for sample fastq files and alignment output files
sample_dir=''
alignment_dir=''

## set reference fasta
bwa_index='UCSC/hg19/genome.fa'

## loop through sequencing lanes
for sample in lane1 lane2 lane3 lane4; do

  ## align to hg19
  bwa mem -t 8 "${bwa_index}" \
    "${sample_dir}"/"${sample}"_R1.fq.gz \
    "${sample_dir}"/"${sample}"_R2.fq.gz \
    > "${alignment_dir}"/"${sample}".sam

  ## create read group
  java -Xmx28g -jar `which AddOrReplaceReadGroups.jar` \
    I="${alignment_dir}"/"${sample}".sam \
    O="${alignment_dir}"/"${sample}"_sorted.bam \
    SORT_ORDER=coordinate \
    VALIDATION_STRINGENCY=LENIENT \
    MAX_RECORDS_IN_RAM=5000000 \
    ID=1 \
    LB=CTTGTA \
    PL=ILLUMINA \
    PU=1234 \
    SM=jurkat

  ## picard mark duplicates
  java -Xmx28g -jar `which MarkDuplicates.jar` \
    I="${alignment_dir}"/"${sample}"_sorted.bam \
    O="${alignment_dir}"/"${sample}"_dedup.bam \
    M="${alignment_dir}"/"${sample}"_metrics.txt \
    ASSUME_SORTED=true \
    VALIDATION_STRINGENCY=LENIENT

  ## index the bam file
  java -Xmx28g -jar `which BuildBamIndex.jar` \
    I="${alignment_dir}"/"${sample}"_dedup.bam

done
