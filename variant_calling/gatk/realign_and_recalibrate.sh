#!/bin/bash
#### GATK indel realignment and base recalibration ####
#### GATK script order: 1                          ####

## exit on error
set -o errexit
## exit on unset variables
set -o nounset

## load software using environment modules
module load gatk/3.3-0

## set directory containing alignment output files
alignment_dir=''

## set locations for external data files that gatk requires
hg19_reference='UCSC/hg19/genome.fa'
dbsnp='GATK/hg19/dbsnp_137.hg19.vcf'
mills='GATK/hg19/Mills_and_1000G_gold_standard.indels.hg19.vcf'
g1000='GATK/hg19/1000G_phase1.indels.hg19.vcf'

## iterate through sequencing lanes
for sample in lane1 lane2 lane3 lane4; do

  ## gatk realigner target creator
  java -Xmx30g -jar `which GenomeAnalysisTK.jar` \
    -T RealignerTargetCreator -nt 8 \
    -R "${hg19_reference}" \
    -I "${alignment_dir}"/"${sample}"_dedup.bam \
    -o "${alignment_dir}"/"${sample}".intervals \
    -known "${mills}" \
    -known "${g1000}"

  ## gatk indel realignment
  java -Xmx30g -jar `which GenomeAnalysisTK.jar` \
    -T IndelRealigner \
    -R "${hg19_reference}" \
    -I "${alignment_dir}"/"${sample}"_dedup.bam \
    -targetIntervals "${alignment_dir}"/"${sample}".intervals \
    -o "${alignment_dir}"/"${sample}"_realigned.bam \
    -known "${mills}" \
    -known "${g1000}"

  ## gatk base recalibrator
  java -Xmx30g -jar `which GenomeAnalysisTK.jar` \
    -T BaseRecalibrator -nct 8 \
    -R "${hg19_reference}" \
    -I "${alignment_dir}"/"${sample}"_realigned.bam  \
    -o "${alignment_dir}"/"${sample}"_recal_data.table \
    -knownSites "${dbsnp}" \
    -knownSites "${mills}" \
    -knownSites "${g1000}"

  ## gatk apply recalibration
  java -Xmx30g -jar `which GenomeAnalysisTK.jar` \
    -T PrintReads -nct 8 \
    -R "${hg19_reference}" \
    -I "${alignment_dir}"/"${sample}"_realigned.bam \
    -BQSR "${alignment_dir}"/"${sample}"_recal_data.table \
    -o "${alignment_dir}"/"${sample}"_recal.bam

done
