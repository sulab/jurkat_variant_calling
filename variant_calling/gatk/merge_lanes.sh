#!/bin/bash
#### merge lane bam files ####
#### GATK script order: 2 ####

## exit on error
set -o errexit
## exit on unset variables
set -o nounset

## load samtools using environment modules
module load samtools

## set directory with alignment results
alignment_dir=''

## change to alignment directory
cd "${alignment_dir}"

## merge lane bams
samtools merge \
  merged.bam \
  lane1_recal.bam \
  lane2_recal.bam \
  lane3_recal.bam \
  lane4_recal.bam
