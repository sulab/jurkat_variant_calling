#!/bin/bash
#### GATK variant quality score recalibrator ####
#### GATK script order: 5                    ####

## exit on error
set -o errexit
## exit on unset variables
set -o nounset

## load software using environment modules
module load R/3.1.1
module load gatk/3.3-0

## set directory containing gatk variant calls
variant_dir='/gpfs/group/su/lhgioia/jurkat/results/gatk/recalibration'
## set directory for recalibration files
recal_dir='/gpfs/group/su/lhgioia/jurkat/results/gatk/recalibration'

## set sample name
sample='merged'

## set locations for external data files required by gatk
hg19_reference='UCSC/hg19/genome.fa'
hapmap='GATK/hg19/hapmap_3.3.hg19.vcf'
omni='GATK/hg19/1000G_omni2.5.hg19.vcf'
KG='GATK/hg19/1000G_phase1.snps.high_confidence.hg19.sites.vcf'
dbsnp='GATK/hg19/dbsnp_137.hg19.vcf'
mills='GATK/hg19/Mills_and_1000G_gold_standard.indels.hg19.vcf'

## gatk snp recalibrator
java -xmx28g -jar `which GenomeAnalysisTK.jar` \
   -T VariantRecalibrator \
   -R "${hg19_reference}" \
   -input "$variant_dir"/"${sample}".vcf \
   -nt 8 \
   -resource:hapmap,known=false,training=true,truth=true,prior=15.0 $hapmap \
   -resource:omni,known=false,training=true,truth=true,prior=12.0 $omni \
   -resource:1000G,known=false,training=true,truth=false,prior=10.0 $KG \
   -resource:dbsnp,known=true,training=false,truth=false,prior=2.0 $dbsnp \
   -an DP -an QD -an MQ -an MQRankSum -an ReadPosRankSum -an FS -an SOR \
   -mode SNP \
   -tranche 100.0 -tranche 99.9 -tranche 99.0 -tranche 90.0 \
   -recalFile "${recal_dir}"/"${sample}"_recalibrate_SNP.recal \
   -tranchesFile "${recal_dir}"/"${sample}"_recalibrate_SNP.tranches \
   -rscriptFile "${recal_dir}"/"${sample}"_recalibrate_SNP_plots.R

## apply snp recalibration
java -Xmx28g -jar `which GenomeAnalysisTK.jar` \
   -T ApplyRecalibration \
   -R "${hg19_reference}" \
   -input "${variant_dir}"/"${sample}".vcf \
   -tranchesFile "${recal_dir}"/"${sample}"_recalibrate_SNP.tranches \
   -recalFile "${recal_dir}"/"${sample}"_recalibrate_SNP.recal \
   -o "${recal_dir}"/"${sample}"_recalibrate_SNP.vcf \
   --ts_filter_level 99.9 \
   -mode SNP

## gatk indel recalibrator
java -Xmx28g -jar `which GenomeAnalysisTK.jar` \
   -T VariantRecalibrator \
   -R "${hg19_reference}" \
   -input "${recal_dir}"/"${sample}"_recalibrate_SNP.vcf \
   --maxGaussians 4 \
   -resource:mills,known=false,training=true,truth=true,prior=12.0 $mills \
   -resource:dbsnp,known=true,training=false,truth=false,prior=2.0 $dbsnp \
   -an QD -an DP -an FS -an SOR -an ReadPosRankSum -an MQRankSum \
   -mode INDEL \
   -tranche 100.0 -tranche 99.9 -tranche 99.0 -tranche 90.0 \
   -recalFile "${recal_dir}"/"${sample}"_recalibrate_INDEL.recal \
   -tranchesFile "${recal_dir}"/"${sample}"_recalibrate_INDEL.tranches \
   -rscriptFile "${recal_dir}"/"${sample}"_recalibrate_INDEL_plots.R

## apply indel recalibration
java -Xmx28g -jar `which GenomeAnalysisTK.jar` \
   -T ApplyRecalibration \
   -R "${hg19_reference}" \
   -input "${recal_dir}"/"${sample}"_recalibrate_SNP.vcf \
   -tranchesFile "${recal_dir}"/"${sample}"_recalibrate_INDEL.tranches \
   -recalFile "${recal_dir}"/"${sample}"_recalibrate_INDEL.recal \
   -o "${recal_dir}"/"${sample}"_recal_SNP_INDEL.vcf \
   --ts_filter_level 99.9 \
   -mode INDEL
