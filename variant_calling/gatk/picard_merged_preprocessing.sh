#!/bin/bash
#### picard preprocessing ####
#### GATK script order: 3 ####

## exit on error
set -o errexit
## exit on unset variables
set -o nounset

## load picard and samtools using environment modules
module load bwa/0.7.10
module load picard/1.103
module load samtools/1.0

## set alignment results directory and sample name
alignment_dir=''
sample='merged'

## sort bam file
java -Xmx28g -jar `which SortSam.jar` \
	I="${alignment_dir}"/"${sample}".bam \
	O="${alignment_dir}"/"${sample}"_sorted.bam \
	SORT_ORDER=coordinate

## picard mark duplicates
java -Xmx28g -jar `which MarkDuplicates.jar` \
	I="${alignment_dir}"/"${sample}"_sorted.bam \
	O="${alignment_dir}"/"${sample}"_dedup.bam \
	M="${alignment_dir}"/"$sample}"_metrics.txt \
	ASSUME_SORTED=true \
	VALIDATION_STRINGENCY=LENIENT


## index the bam file
java -Xmx28g -jar `which BuildBamIndex.jar` \
	I="${alignment_dir}"/"${sample}"_dedup.bam
