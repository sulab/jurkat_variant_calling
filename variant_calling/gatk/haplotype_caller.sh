#!/bin/bash
#### GATK haplotype caller ####
#### GATK script order: 4  ####

## exit on error
set -o errexit
## exit on unset variables
set -o nounset

## load gatk using environment modules
module load gatk/3.3-0

## set directory containing alignment output files
alignment_dir=''
## set directory for gatk variant calls
variant_dir=''
## set sample name
sample='merged'

## set locations for external data files required by gatk
hg19_reference='UCSC/hg19/genome.fa'
dbsnp='GATK/hg19/dbsnp_137.hg19.vcf' 
mills='GATK/hg19/Mills_and_1000G_gold_standard.indels.hg19.vcf'
g1000='GATK/hg19/1000G_phase1.indels.hg19.vcf'

## ploidy will need to be changed
java -Xmx28g -jar `which GenomeAnalysisTK.jar` \
	-T HaplotypeCaller \
	-nct 8 \
	-R "${hg19_reference}" \
	-ploidy 1 \
	--dbsnp "${dbsnp}" \
	-I "${alignment_dir}"/"${sample}"_dedup.bam \
	--genotyping_mode DISCOVERY \
	-stand_emit_conf 10 \
	-stand_call_conf 30 \
	-o "${variant_dir}"/"${sample}"_jurkat.vcf
