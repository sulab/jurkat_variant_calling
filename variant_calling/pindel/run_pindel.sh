#!/bin/bash
#### Pindel run script      ####
#### Pindel script order: 1 ####

## exit on error
set -o errexit
## exit on unset variables
set -o nounset

## load software using environment variables
module load pindel/0.2.5

## set directories for alignment input data and pindel results
alignment_dir=''
pindel_dir=''

## set location for hg19 reference fasta
hg19='UCSC/hg19/genome.fa'

## run from pindel results directory
cd "${pindel_dir}"

## run pindel
## the path to the .bam file needs to be added to the config file
pindel -f "${hg19}" -i "${pindel_dir}"/jurkat_config.txt -T 8 -o "${pindel_dir}"/jurkat
