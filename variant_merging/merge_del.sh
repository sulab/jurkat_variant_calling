#!/bin/bash
#### Merge deletion variant calls                 ####
#### BedTools version 2.25.0 was used for testing ####

## exit on error
set -o errexit
## exit on unset variables
set -o nounset

## set bedtools directory
bedtools_dir=''

## set results directory for each tool
gatk_dir=''
pindel_dir=''
breakdancer_dir=''
cnvnator_dir=''

## merge SHORT Deletions
## merge gatk and pindel
cat $gatkDIR/assTetra_del_short.bed pindel/jurkat_D_filtered_merged_short.bed | sort -k1,1 -k2,2n > sv_compare/gatk_pindel_del_short_cat.bed

$bedtools_dir/bedtools merge -i sv_compare/gatk_pindel_del_short_cat.bed > sv_compare/gatk_pindel_del_short_merged.bed

# remove breakdancer that overlap gatk/pindel and merge
$bedtools_dir/bedtools intersect -v -a breakdancer/jurkat_breakdancer_80_merged_del_short.bed -b sv_compare/gatk_pindel_del_short_merged.bed > sv_compare/breakdancer_del_short_noGP.bed

cat sv_compare/gatk_pindel_del_short_merged.bed sv_compare/breakdancer_del_short_noGP.bed | sort -k1,1 -k2,2n > sv_compare/del_short_all_cat.bed

$bedtools_dir/bedtools merge -i sv_compare/del_short_all_cat.bed > sv_merge/del_short_merged.bed


## merge Long Deletions
# merge gatk and pindel
cat $gatkDIR/assTetra_del_long.bed pindel/jurkat_D_filtered_merged_long.bed | sort -k1,1 -k2,2n > sv_compare/gatk_pindel_del_long_cat.bed

$bedtools_dir/bedtools merge -i sv_compare/gatk_pindel_del_long_cat.bed > sv_compare/gatk_pindel_del_long_merged.bed

# remove breakdancer that overlap gatk/pindel and merge
$bedtools_dir/bedtools intersect -v -a breakdancer/jurkat_breakdancer_80_merged_del_long.bed -b sv_compare/gatk_pindel_del_long_merged.bed > sv_compare/breakdancer_del_long_noGP.bed

cat sv_compare/gatk_pindel_del_long_merged.bed sv_compare/breakdancer_del_long_noGP.bed | sort -k1,1 -k2,2n > sv_compare/del_long_gp_bd_cat.bed

$bedtools_dir/bedtools merge -i sv_compare/del_long_gp_bd_cat.bed > sv_compare/del_long_gp_bd_merged.bed

# remove cnvnator that overlap gatk/pindel/breakdancer and merge
$bedtools_dir/bedtools intersect -v -a cnvnator/jurkat_cnvnator_merged_del.bed -b sv_compare/del_long_gp_bd_merged.bed > sv_compare/cnvnator_del_long_noGP_noBD.bed

cat sv_compare/del_long_gp_bd_merged.bed sv_compare/cnvnator_del_long_noGP_noBD.bed | sort -k1,1 -k2,2n > sv_compare/del_long_all_cat.bed

$bedtools_dir/bedtools merge -i sv_compare/del_long_all_cat.bed > sv_merge/del_long_merged.bed
